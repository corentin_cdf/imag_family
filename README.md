# imag_family

Créons ensemble un immense graphe de famille de l'Ensimag !

## Pré-requis
* Pour générer le graphe : installer Graphviz (et l'ajouter à votre PATH) : https://graphviz.gitlab.io/download/ (NB : déjà installé sur les machines de l'imag, normalement)
* Pour lancer les scripts Python : installer Python 3, ainsi que la librairie `graphviz` (`pip install graphviz` si vous avez pip installé sur votre machine)

## Utilisation
* Pour générer le graphe : utiliser `draw.sh` ou `draw.bat`, sinon exécuter la commande : `dot -Tpng imag_family.gv -o imag_family.png`
* Pour extraire la lignée d'une personne : lancer `extract.py` avec le nom de la personne en argument et entre guillemets. Exemple : `extract.py "Obi-Wan Kenobi"`.

## Contribution
Pour ajouter une personne ou une relation de parrain/marraine à fillot(te) : écrire directement dans le fichier `imag_family.gv`. Ce fichier décrit le graphe "ensimag", constitué de :
* sous-graphes (les promos)
* noeuds (les gens)
* arcs (lien de parrain/marraine vers fillot(te))

**Pour ajouter une personne** : dans le subgraph (promo) qui lui correspond, écrire une ligne avec son prénom + nom entre guillemets (ex: `"Obi-Wan Kenobi"` dans le sous-graphe `1999`).

**Pour ajouter une relation** : sous le subgraph (promo) du parrain/marraine, écrire une ligne de la forme : "PrénomParrain NomParrain" -> "PrénomFillot NomFillot" (ex : `"Obi-Wan Kenobi" -> "Anakin Skywalker"`)

Trucs auxquels il faut faire attention :
* l'orthographe et la casse des noms : majuscules, accents, tirets etc. Les couples prénom/nom sont les identifiants des noeuds donc c'est important pour éviter les doublons.
* quand on ajoute une relation, ne pas oublier d'ajouter les deux personnes dans leurs sous-graphes respectifs (si elles n'y sont pas encore)
* grouper les relations par parrain/marraine (pour mieux s'y retrouver)
* trier dans l'ordre alphabétique des prénoms, que ce soit entre les personnes d'une promo ou parmi les fillot(te)s d'une personne (pour mieux s'y retrouver)

## Documentations
* langage DOT et logiciel Graphviz : http://www.graphviz.org/doc/info/
* langage DOT (Wikipedia) : https://en.wikipedia.org/wiki/DOT_(graph_description_language)
* librairie Python graphviz : https://graphviz.readthedocs.io/en/stable/index.html
